import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {map, Observable, tap} from 'rxjs';
import {Product} from '../common/product';
import {ProductCategory} from '../common/product-category';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private baseUrl = 'http://localhost:8080/api/products';
  private categoryUrl = 'http://localhost:8080/api/categories';

  constructor(private httpClient: HttpClient) {
  }

  getProductListPaginate(thePage: number,
                         thePageSize: number): Observable<GetResponseProducts> {
    const searchUrl = `${this.baseUrl}?page=${thePage}&size=${thePageSize}`;
    console.log(`searchUrl=${searchUrl}`)
    return this.httpClient.get<GetResponseProducts>(searchUrl);
  }

  getProductListByCategoryPaginate(thePage: number,
                         thePageSize: number,
                         theCategoryId: number): Observable<GetResponseProducts> {
    const searchUrl = `${this.baseUrl}/category/${theCategoryId}?page=${thePage}&size=${thePageSize}`;
    console.log(`searchUrl=${searchUrl}`)
    return this.httpClient.get<GetResponseProducts>(searchUrl);
  }

  getProductList(theCategoryId: number): Observable<Product[]> {
    const searchUrl = `${this.baseUrl}/category/${theCategoryId}`;
    return this.getProducts(searchUrl);
  }

  searchProducts(theKeyword: string): Observable<Product[]> {
    const searchUrl = `${this.baseUrl}?name=${theKeyword}`;
    return this.getProducts(searchUrl);
  }

  searchProductPaginate(thePage: number,
                         thePageSize: number,
                         theKeyword: string): Observable<GetResponseProducts> {
    const searchUrl = `${this.baseUrl}?name=${theKeyword}&page=${thePage}&size=${thePageSize}`;
    console.log(`searchUrl=${searchUrl}`)
    return this.httpClient.get<GetResponseProducts>(searchUrl);
  }

  private getProducts(searchUrl: string) {
    return this.httpClient.get<GetResponseProducts>(searchUrl).pipe(
      tap((response: GetResponseProducts) => console.log(response)),
      map(response => response.content)
    );
  }

  getProductCategories(): Observable<ProductCategory[]> {
    return this.httpClient.get<GetResponseProductCategory>(this.categoryUrl).pipe(
      tap((response: GetResponseProductCategory) => console.log(response)),
      map(response => response.content)
    );
  }

  getProduct(theProductId: number): Observable<Product> {
    const productUrl = `${this.baseUrl}/${theProductId}`;

    console.log(productUrl);

    return this.httpClient.get<Product>(productUrl);
  }
}

interface GetResponseProducts {
  content: Product[],
  pageable: {
    pageNumber: number,
    pageSize: number
  },
  totalElements: number
}

interface GetResponseProductCategory {
  content: ProductCategory[];
}
