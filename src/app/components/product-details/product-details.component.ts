import {Component, OnInit} from '@angular/core';
import {Product} from "../../common/product";
import {ProductService} from "../../services/product.service";
import {ActivatedRoute, RouterLink} from "@angular/router";
import {CurrencyPipe} from "@angular/common";
import {CartService} from "../../services/cart.service";
import {CartItem} from "../../common/cart-item";

@Component({
  selector: 'app-product-details',
  standalone: true,
  imports: [
    CurrencyPipe,
    RouterLink
  ],
  templateUrl: './product-details.component.html',
  styleUrl: './product-details.component.css'
})
export class ProductDetailsComponent implements OnInit {

  product!: Product;

  constructor(private productService: ProductService,
              private cartService: CartService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
        this.handleProductDetails();
      }
    );
  }

  private handleProductDetails() {

    console.log("this.route.snapshot - " + this.route.snapshot);
    console.log("this.route.snapshot.paramMap - " + this.route.snapshot.paramMap);
    console.log("this.route.snapshot.paramMap.get(\"id\")! - " + this.route.snapshot.paramMap.get("id")!);

    const theProductId = +this.route.snapshot.paramMap.get("id")!;

    this.productService.getProduct(theProductId).subscribe(
      data => {
        this.product = data
      }
    );
  }

  addToCart() {
    console.log(`Adding to a cart: ${this.product.name}, ${this.product.unitPrice}`);

    const cartItem: CartItem = new CartItem(this.product);
    this.cartService.addToCart(cartItem);
  }
}

